// const books = [
//     { 
//       author: "Люсі Фолі",
//       name: "Список запрошених",
//       price: 70 
//     }, 
//     {
//      author: "Сюзанна Кларк",
//      name: "Джонатан Стрейндж і м-р Норрелл",
//     }, 
//     { 
//       name: "Дизайн. Книга для недизайнерів.",
//       price: 70
//     }, 
//     { 
//       author: "Алан Мур",
//       name: "Неономікон",
//       price: 70
//     }, 
//     {
//      author: "Террі Пратчетт",
//      name: "Рухомі картинки",
//      price: 40
//     },
//     {
//      author: "Анґус Гайленд",
//      name: "Коти в мистецтві",
//     }
//   ];
// const root = document.querySelector("#root")
// root.insertAdjacentHTML("beforeend",`<ul>${books.map((item)=>{
//     try{
//         if(!item.name){
//             throw new Error ("noname") 
//         }
//         if(!item.author){
//             throw new Error ("no author") 
//         }
//         if(!item.price){
//             throw new Error ("no price") 
//         }
//         return `<li>${item.name}${item.author}${item.price}</li>`
//     }
//     catch(error){

//         console.error(error)
//     }
// }).join("")}</ul>`)


const books = [
  {
    author: "Люсі Фолі",
    name: "Список запрошених",
    price: 70
  },
  {
    author: "Сюзанна Кларк",
    name: "Джонатан Стрейндж і м-р Норрелл",
  },
  {
    name: "Дизайн. Книга для недизайнерів.",
    price: 70
  },
  {
    author: "Алан Мур",
    name: "Неономікон",
    price: 70
  },
  {
    author: "Террі Пратчетт",
    name: "Рухомі картинки",
    price: 40
  },
  {
    author: "Анґус Гайленд",
    name: "Коти в мистецтві",
  }
];
const root = document.querySelector("#root")
const ul = document.createElement("ul")
root.append(ul)

const renderBooks = (books) => {
  books.forEach(book => {
  const [validationResult,emptyFields] = validateFields(book)
  try{
    if(!validationResult){
      throw new Error(`no fields:${emptyFields.join(", ")}`)
    }else{
      createElement(book)
    }
  }catch(error){
  console.error(error)}
  });
}

const validateFields = (book) => {
const necessaryFields = ["author","name","price"]
const bookFields = Object.keys(book)
let validationResult = true
const emptyFields = []
if(necessaryFields.length !== bookFields.length){
  validationResult = false
}
necessaryFields.forEach((item)=>{
  const isFieldsInBook = bookFields.includes(item)
  if(!isFieldsInBook){
    emptyFields.push(item)
    validationResult = false
  }
})
return [validationResult,emptyFields]
}
const createElement = (book) => {
 ul.insertAdjacentHTML("beforeend",`<li>${book.author} ${book.name} ${book.price}</li>`)
}

renderBooks(books)



// const object = {
//   name:"ivan",
//   age:13,
//   city:"Kyiv",
//   country:"poland"
// }
// const asd = {...object}
// const {name,age:year,city,country= "ukraine"} = object  
// console.log(name,year,country )
// const users = [{
//   name:"ivan",
//   age:13,
//   city:"Kyiv",
//   country:"poland"
// },
// {
//   name:"lesha",
//   age:15,
//   city:"Kyiv",
//   country:"viena"
// }]
// const smthng = users.map(({name,age,city,country})=>{
// return `${name} ${age} ${city} ${country}`
// })

// const  array = [1,2,3,4,5]
// const [a,b,,,d,] = array
// const [g,,u,...rest] = array
// console.log(g,u,rest)
// const array2 = [4,5,2,4,6]
// const newArr = array.concat(array2)
// const array3 = [...array,...array2]
// const newArray = [...new Set([...array,...array2]
// )]
// console.log(newArray)